#!/bin/bash -x
# vim: ai:ts=8:sw=8:noet
# This is shell provisioner for Kitchen
set -eufo pipefail
IFS=$'\t\n'

export DEBIAN_FRONTEND="noninteractive"
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'No configuration'"

# since gitlab-vault already in repo, install it from there
# and then install the version being tested with dpkg
# lazy way to test repo and fetch all dependencies
echo 'deb http://aptly.gitlab.com/ci xenial main' > /etc/apt/sources.list.d/aptly.gitlab.com.list
# TODO: a proper ssl from a proper CA, but for now just embed the fp here
wget -O /tmp/apt.key http://aptly.gitlab.com/release.asc
echo 'f4fd8d5bc50ab62b42ea6092867a61a9cfe2961c8629ec4591396c7e6e7ce7ab  /tmp/apt.key' | sha256sum --status --check
# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
	echo "MiTM in progress, refusing to continue"
	exit $?
fi

apt-key add /tmp/apt.key
apt-get -qq update

apt-get -yqq install mailutils gnupg2	# for notification
dpkg -i /tmp/kitchen/data/gitlab-vault_0.0.3.deb
apt-get -qqf install	# install all dependencies

### main flow
# Init the vault, and send individual keys on email
# NOTE: no spaces/commas in filenames supported, trailing comma yields stacktrace too
KEYS="$(find /etc/vault/unseal_pubkeys -type f -name '*.asc' | sort | tr '\n' ',' | sed 's/,$//')"
# for further mapping to emails (added [0] element for conveninence)
IFS="," read -ra KEYS_A <<< "skip_zero,${KEYS}"
### helper functions
function _notify() {
	# gets vault init output file as $1
	# Example lines we're interested in:
	# Unseal key N: KEY_DATA
	# or
	# Initial Root Token: TOKEN_DATA
	# extracts email from file specified as KEYS_A[N] and sends KEY_DATA to it
	# if not on CI
	while IFS=" " read -r a b n data; do
		if [[ "${a} ${b} ${n}" == "Initial Root Token:" ]]; then
			# TODO: encrypt this with separate key and send to ops-contact email
			echo "IRT: ${data}" | mailx -s "initial root token" "ilya+vault@gitlab.com"
			continue
		fi

		if [[ "${a} ${b}" == "Unseal Key" ]]; then
			n="${n//:}"			# remove colon
			f="${KEYS_A[$n]}"		# lookup key file
			email="$(base64 -d "${f}" | \
				gpg2 --list-packets --verbose | \
				sed -ne 's/.* <\(.*+vault@gitlab.com\)>"$/\1/p')" # NOTE: only match strict format: +vault gitlab email, at the end of the line

			if [[ "x" == "x${email}" ]]; then
				echo "empty email in file ${f}, bailing out!"
				continue
			fi

			echo "Your Vault unseal token is: ${data}" | mailx -s "unseal token" "${email}"
		fi
	done < "$1"
}
export -f _notify

# NOTE: ${KEYS} should be unquoted for correct length calculation
# shellcheck disable=SC2086
VAULT_ADDR="http://127.0.0.1:8200/" vault init \
	-key-shares="$(IFS=,; set -f; set -- ${KEYS}; echo $#)"  \
	-key-threshold=2 \
	-pgp-keys="${KEYS}" \
	-root-token-pgp-key="/etc/vault/unseal_pubkeys/ilya.asc" > /tmp/init_output

# process init output and notify users
_notify /tmp/init_output

# TODO: separate key for root token
