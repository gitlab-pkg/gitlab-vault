# encoding: utf-8

# InSpec tests for gitlab-vault package

control 'general-pkg-checks' do
  impact 1.0
  title 'General tests for gitlab-vault package'
  desc '
    This control ensures that:
      * gitlab-vault package version 0.0.1 is installed

  '
  describe package('gitlab-vault') do
    it { should be_installed }
    its ('version') { should eq '0.0.3' }
  end
end

control 'general-service-checks' do
  impact 1.0
  title 'General tests for vault service'
  desc '
    This control ensures that:
      * vault service is installed, enabled and running
      * vault is listening on 0.0.0.0:8200
  '
  describe service('vault') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe port('8200') do
    its('processes') { should eq ['vault'] }
    its('protocols') { should eq ['tcp'] }
    its('addresses') { should eq ['0.0.0.0'] }
  end
end

control 'vault-checks' do
  impact 1.0
  title 'Vault tests as a service'
  desc '
    This control ensures that:
      * vault service is inited
  '
  describe bash('VAULT_ADDR=http://127.0.0.1:8200/ vault init -check') do
    its('exit_status') { should eq 0 }
    its('stdout.strip') { should eq 'Vault has been initialized' }
    its('stderr') { should eq '' }
  end
end
